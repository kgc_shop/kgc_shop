package com.kgc.shop.service;

import com.kgc.shop.dto.CommentDto;
import com.kgc.shop.dto.CommentExample;
import com.kgc.shop.mapper.CommentMapper;
import com.kgc.shop.utils.PageUtils;
import com.kgc.shop.vo.CommentVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by XuHui on 2020/9/22
 */
@Service
public class CommentService {
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private ExUserService exUserService;

    /**
     * 查询评论详情(首页默认一条)
     *
     * @param pageNo
     * @param pageSize
     * @param itemId
     * @return
     */
    public PageUtils<CommentVo> getComment(Integer pageNo, Integer pageSize, String itemId) {
        if (pageNo == null){
            pageNo = 1;
        }
        if (pageSize == null){
            pageSize = 4;
        }
        
        PageUtils pageUtils = new PageUtils();
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setPageNo(pageNo);

        CommentExample commentExample = new CommentExample();
        long commentCount = commentMapper.countByExample(commentExample);
        List<CommentDto> commentDtos = commentMapper.queryItemsComments(itemId,pageUtils.getPageNo(),pageSize);

        List<CommentVo> commentVos = new ArrayList<CommentVo>();
        commentDtos.forEach(commentDto -> {
            CommentVo commentVo = new CommentVo();
            String userId = commentDto.getUserId();
            String userName = exUserService.getUserName(userId);
            String userPic = exUserService.getUser(userId).getUserPic();
            commentDto.setUserName(userName);
            commentDto.setUserPic(userPic);
            BeanUtils.copyProperties(commentDto, commentVo);
            //评论时间
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            commentVo.setCreateTime(simpleDateFormat.format(commentDto.getCreatetime()));
            commentVos.add(commentVo);
        });

        pageUtils.setTotalCount(commentCount);
        pageUtils.setCurrentList(commentVos);
        return pageUtils;

    }

}
