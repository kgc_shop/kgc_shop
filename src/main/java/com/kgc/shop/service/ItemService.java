package com.kgc.shop.service;

import com.kgc.shop.dto.*;
import com.kgc.shop.mapper.*;
import com.kgc.shop.utils.UuidUtils;
import com.kgc.shop.vo.ExCouponVo;
import com.kgc.shop.vo.ExItemsVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by XuHui on 2020/9/21
 */
@Service
public class ItemService {
    @Autowired
    private ExItemsMapper exItemsMapper;
    
    @Autowired
    private ExItemcouponMapper exItemcouponMapper;
    
    @Autowired
    private ExCouponMapper exCouponMapper;
    
    @Autowired
    private CouponService couponService;

    /**
     * 查询商品详情
     *
     * @param itemId
     * @return
     */
    public ExItemsVo queryGoods(String itemId) {
        ExItemsDto exItemsDto = exItemsMapper.selectByItemId(itemId);
        String picDetail = exItemsDto.getPicDetail();
        ExItemsVo exItemsVo = new ExItemsVo();
        BeanUtils.copyProperties(exItemsDto, exItemsVo);
        String[] picDetailList = picDetail.split(";");
        exItemsVo.setPicDetailList1(picDetailList[0]);
        exItemsVo.setPicDetailList2(picDetailList[1].substring(2));
        exItemsVo.setPicDetailList3(picDetailList[2].substring(2));
        exItemsVo.setPicDetailList4(picDetailList[3].substring(2));
        exItemsVo.setPicDetailList5(picDetailList[4].substring(2));
        return exItemsVo;
    }

    /**
     * 查询商品可用优惠券
     *
     * @param itemId
     * @return
     */
    public List<ExCouponVo> getCoupon(String itemId) {
        ExItemcouponExample exItemcouponExample = new ExItemcouponExample();
        exItemcouponExample.createCriteria().andItemIdEqualTo(itemId);
        List<ExItemcouponDto> exItemcouponDtos = exItemcouponMapper.selectByExample(exItemcouponExample);
        List<ExCouponVo> exCouponVos = new ArrayList<ExCouponVo>();
        exItemcouponDtos.forEach(exItemcouponDto -> {
            String couponId = exItemcouponDto.getCouponId();
            ExCouponExample exCouponExample = new ExCouponExample();
            exCouponExample.createCriteria().andCouponIdEqualTo(couponId);
            ExCoupon exCoupon = exCouponMapper.selectByExample(exCouponExample).get(0);
            ExCouponVo exCouponVo = new ExCouponVo();
            BeanUtils.copyProperties(exCoupon, exCouponVo);
            exCouponVos.add(exCouponVo);
        });
        return exCouponVos;
    }

    @Autowired
    private ExOrderdetailMapper exOrderdetailMapper;
    @Autowired
    private ItemService itemService;
    @Autowired
    private ExCartdetailMapper exCartdetailMapper;
    @Autowired
    private ExOrderMapper exOrderMapper;
    
}
