package com.kgc.shop.service;

import com.kgc.shop.dto.ReceiverInfoDto;
import com.kgc.shop.dto.ReceiverInfoExample;
import com.kgc.shop.mapper.ReceiverInfoMapper;
import com.kgc.shop.params.AddressParam;
import com.kgc.shop.vo.ReceiverInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by XuHui on 2020/9/25
 */
@Service
public class AddressService {
    @Autowired
    private ReceiverInfoMapper receiverInfoMapper;

    /**
     * 查询当前用户下的所有收货地址
     *
     * @param userId
     * @return
     */
    public List<ReceiverInfoVo> queryAllAddress(String userId) {
        ReceiverInfoExample receiverInfoExample = new ReceiverInfoExample();
        receiverInfoExample.createCriteria().andUserIdEqualTo(userId);
        List<ReceiverInfoDto> receiverInfoDtos = receiverInfoMapper.selectByExample(receiverInfoExample);
        List<ReceiverInfoVo> receiverInfoVos = new ArrayList<ReceiverInfoVo>();
        receiverInfoDtos.forEach(receiverInfoDto -> {
            ReceiverInfoVo receiverInfoVo = new ReceiverInfoVo();
            BeanUtils.copyProperties(receiverInfoDto, receiverInfoVo);
            receiverInfoVos.add(receiverInfoVo);
        });
        return receiverInfoVos;
    }

    /**
     * 查询地址详细信息
     *
     * @param id
     */
    public ReceiverInfoVo queryAddrDetail(Integer id) {
        ReceiverInfoDto receiverInfoDto = receiverInfoMapper.selectByPrimaryKey(id);
        ReceiverInfoVo receiverInfoVo = new ReceiverInfoVo();
        BeanUtils.copyProperties(receiverInfoDto, receiverInfoVo);
        return receiverInfoVo;
    }

    /**
     * 添加用户收货地址
     *
     * @param addressParam
     */
    public void addAddress(AddressParam addressParam) {
        ReceiverInfoDto receiverInfoDto = new ReceiverInfoDto();
        BeanUtils.copyProperties(addressParam, receiverInfoDto);
        receiverInfoMapper.insert(receiverInfoDto);
    }

    /**
     * 修改收货地址详细信息
     *
     * @param receiverInfoVo
     */
    public void modAddress(ReceiverInfoVo receiverInfoVo) {
        ReceiverInfoDto receiverInfoDto = new ReceiverInfoDto();
        BeanUtils.copyProperties(receiverInfoVo, receiverInfoDto);
        receiverInfoMapper.updateByPrimaryKeySelective(receiverInfoDto);
    }

    /**
     * 删除收货地址
     *
     * @param id
     */
    public void delAddress(Integer id) {
        receiverInfoMapper.deleteByPrimaryKey(id);
    }
}
