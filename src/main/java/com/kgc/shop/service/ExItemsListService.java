package com.kgc.shop.service;

import com.kgc.shop.dto.*;
import com.kgc.shop.mapper.CommentMapper;
import com.kgc.shop.mapper.ExItemsMapper;
import com.kgc.shop.mapper.ExItemtypeMapper;
import com.kgc.shop.mapper.ExShoptypeMapper;
import com.kgc.shop.utils.PageUtils;
import com.kgc.shop.vo.ExItemIndextypeVo;
import com.kgc.shop.vo.ExItemsIndexVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jialin
 * @createTime 2020-09-23
 */
@Service
public class ExItemsListService {
    @Autowired
    private ExItemsMapper exItemsMapper;

    @Autowired
    private ExItemtypeMapper exItemtypeMapper;
    
    @Autowired
    private ExShoptypeMapper exShoptypeMapper;
    
    @Autowired
    private CommentMapper commentMapper;
    

    public List<ExItemIndextypeVo> selectType() {
        List<ExItemIndextypeVo> exItemtypeVos = new ArrayList<>();
        ExItemtypeExample exItemtypeExample = new ExItemtypeExample();
        exItemtypeExample.createCriteria().andIdIsNotNull();

        List<ExItemtype> exItemtypeDtos = exItemtypeMapper.selectByExample(exItemtypeExample);
        for (ExItemtype exItemtypeDto : exItemtypeDtos) {
            ExItemIndextypeVo exItemtypeVo = new ExItemIndextypeVo();
            BeanUtils.copyProperties(exItemtypeDto, exItemtypeVo);
            exItemtypeVos.add(exItemtypeVo);
        }
        return exItemtypeVos;

    }

    public PageUtils<ExItemsIndexVo> select(String itemName, Integer itemType,
                                       Integer pageNo, Integer pageSize) {

        if (pageNo == null) {
            pageNo = 1;
        }
        if (pageSize == null) {
            pageSize = 4;
        }

        PageUtils pageUtils = new PageUtils();
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setPageNo(pageNo);

        ExItemsExample exItemsExample = new ExItemsExample();

        if (itemType != null) {
            exItemsExample.createCriteria().andItemTypeEqualTo(itemType);
        } else if (!StringUtils.isEmpty(itemName)) {
            exItemsExample.createCriteria().andItemNameLike("%" + itemName + "%");
        } else {
            exItemsExample.createCriteria().andItemNameIsNotNull().andItemTypeEqualTo(1);
        }
        long exItemsCount = exItemsMapper.countByExample(exItemsExample);
        pageUtils.setTotalCount(exItemsCount);

        //分页查询
        List<ExItemsDto> exItemsDtos = exItemsMapper.queryIndexItems(itemName, itemType, pageUtils.getPageNo(), pageSize);

        List<ExItemsDto> exItemsDtos1 = exItemsMapper.selectByExample(exItemsExample);

        List<ExItemsIndexVo> exItemsVos = new ArrayList<ExItemsIndexVo>();
        for (ExItemsDto exItemsDto : exItemsDtos) {
            ExItemsIndexVo exItemsVo = new ExItemsIndexVo();
            BeanUtils.copyProperties(exItemsDto, exItemsVo);
            //商品类型
            ExItemtypeExample exItemtypeExample = new ExItemtypeExample();
            exItemtypeExample.createCriteria().andItemTypeEqualTo(exItemsDto.getItemType());
            exItemsVo.setItemTypeName(exItemtypeMapper.selectByExample(exItemtypeExample).get(0).getTypeName());
            //店铺类型
            if (exItemsDto.getShopType() == 0){
                exItemsVo.setShopTypeName("自营");
            }else if (exItemsDto.getShopType() == 1){
                exItemsVo.setShopTypeName("第三方");
            }
            //评价数
            CommentExample commentExample = new CommentExample();
            commentExample.createCriteria().andItemIdEqualTo(exItemsDto.getItemId());
            Long commentCount = commentMapper.countByExample(commentExample);
            exItemsVo.setCommentCount(commentCount);
            exItemsVos.add(exItemsVo);
        }

        pageUtils.setCurrentList(exItemsVos);

        return pageUtils;
    }

}