package com.kgc.shop.service;

import com.kgc.shop.dto.ExItemRepoDto;
import com.kgc.shop.mapper.ExItemRepoMapper;
import com.kgc.shop.vo.ExItemRepoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by XuHui on 2020/9/22
 */
@Service
public class StockService {
    @Autowired
    private ExItemRepoMapper exItemRepoMapper;

    //查询商品库存详情
    public ExItemRepoVo getStock(String itemId) {
        ExItemRepoDto exItemRepoDto = exItemRepoMapper.selectByItemId(itemId);
        ExItemRepoVo exItemRepoVo = new ExItemRepoVo();
        BeanUtils.copyProperties(exItemRepoDto, exItemRepoVo);
        return exItemRepoVo;
    }
}
