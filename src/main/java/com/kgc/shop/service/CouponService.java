package com.kgc.shop.service;

import com.kgc.shop.dto.ExCoupon;
import com.kgc.shop.mapper.ExCouponMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by XuHui on 2020/9/22
 */
@Service
public class CouponService {
    @Autowired
    private ExCouponMapper exCouponMapper;

    /**
     * 查询优惠券详情
     *
     * @param couponId
     */
    public ExCoupon queryCoupon(String couponId) {
        ExCoupon exCouponDto = exCouponMapper.selectByPrimaryKey(couponId);
        return exCouponDto;
    }

}
