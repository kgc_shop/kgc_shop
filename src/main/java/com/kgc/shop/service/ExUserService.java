package com.kgc.shop.service;

import com.kgc.shop.dto.ExCouponExample;
import com.kgc.shop.dto.ExUserDto;
import com.kgc.shop.dto.ExUserExample;
import com.kgc.shop.dto.ExUsercouponDto;
import com.kgc.shop.mapper.ExCouponMapper;
import com.kgc.shop.mapper.ExUserMapper;
import com.kgc.shop.mapper.ExUsercouponMapper;
import com.kgc.shop.vo.ExUserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by XuHui on 2020/9/22
 */
@Service
public class ExUserService {
    @Autowired
    private ExUserMapper exUserMapper;
    @Autowired
    private ExUsercouponMapper exUsercouponMapper;
    @Autowired
    private ExCouponMapper exCouponMapper;

    /**
     * 查询当前用户信息
     *
     * @param UserId
     * @return
     */
    public ExUserVo getUser(String UserId) {
        ExUserDto exUserDto = exUserMapper.selectByPrimaryKey(UserId);
        ExUserVo exUserVo = new ExUserVo();
        BeanUtils.copyProperties(exUserDto, exUserVo);
        return exUserVo;
    }

    /**
     * 通过用户ID查询用户名
     *
     * @param userId
     * @return
     */
    public String getUserName(String userId) {
        ExUserDto exUserDto = exUserMapper.selectByPrimaryKey(userId);
        String userName = exUserDto.getUserName();
        return userName;
    }

    /**
     * 添加用户的优惠券
     *
     * @param couponId
     * @param userId
     */
    public boolean addCoupon(String couponId, String userId) {
        boolean result = false;
        ExUsercouponDto exUsercouponDto = new ExUsercouponDto();
        exUsercouponDto.setCouponId(couponId);
        exUsercouponDto.setUserId(userId);
        int row = exUsercouponMapper.insertSelective(exUsercouponDto);
        if (row > 0) result = true;
        //优惠券生效时间开始，状态设置为1，表示被用户使用了
        exCouponMapper.updateCouponSetStartTime(couponId);
        return result;
    }

    /**
     * 查询用户的收货地址
     * @param userId
     * @return
     */
    public List<String> queryAddress(String userId){
        List<String> address = new ArrayList<>();
        ExUserExample exUserExample = new ExUserExample();
        exUserExample.createCriteria().andUserIdEqualTo(userId);
        //查询用户
        ExUserDto exUserDto = exUserMapper.selectByExample(exUserExample).get(0);
        String[] addresses = exUserDto.getUserAddress().split(";");
        for (String s : addresses) {
            address.add(s);
        }
        return address;
    }

}
