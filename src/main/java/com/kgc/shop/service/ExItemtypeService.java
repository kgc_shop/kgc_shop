package com.kgc.shop.service;/*
package com.kgc.shop.service;

import com.kgc.shop.Vo.ExItemtypeVo;
import com.kgc.shop.dto.ExItemtypeDto;
import com.kgc.shop.dto.ExItemtypeExample;
import com.kgc.shop.mapper.ExItemtypeMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExItemtypeService {
    @Autowired(required = false)
    private ExItemtypeMapper exItemtypeMapper;

    public List<ExItemtypeVo> select(Integer Id){
        List<ExItemtypeVo> exItemtypeVos= new ArrayList<>();
        ExItemtypeExample exItemtypeExample=new ExItemtypeExample();
        exItemtypeExample.createCriteria().andIdIsNotNull();

        List<ExItemtypeDto> exItemtypeDtos=exItemtypeMapper.selectByExample(exItemtypeExample);
        for (ExItemtypeDto exItemtypeDto : exItemtypeDtos) {
            ExItemtypeVo exItemtypeVo =new ExItemtypeVo();
            BeanUtils.copyProperties(exItemtypeDto, exItemtypeVo);
            exItemtypeVos.add(exItemtypeVo);
        }
        return  exItemtypeVos;

    }

}*/
