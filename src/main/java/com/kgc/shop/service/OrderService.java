package com.kgc.shop.service;

import com.kgc.shop.dto.*;
import com.kgc.shop.mapper.*;
import com.kgc.shop.utils.PageUtils;
import com.kgc.shop.utils.UuidUtils;
import com.kgc.shop.vo.OrderListVo;
import com.kgc.shop.vo.OrderPayedListVo;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jialin
 * @createTime 2020-09-21
 */
@Log4j
@Service
public class OrderService {

    @Autowired
    private ExOrderMapper exOrderMapper;

    @Autowired
    private ExOrderdetailMapper exOrderdetailMapper;

    @Autowired
    private ExCartdetailMapper exCartdetailMapper;

    @Autowired
    private ExItemsMapper exItemsMapper;

    @Autowired
    private ExShoptypeMapper exShoptypeMapper;

    @Autowired
    private ItemService itemService;

    @Autowired
    private ExUserMapper exUserMapper;

    /**
     * 根据userId查询所有的已支付订单列表，没有已支付的返回false
     *
     * @param userId
     * @return
     */
    public boolean ifPay(String userId) {
        //根据userId查询所有的已支付订单列表
        ExOrderExample exOrderExample = new ExOrderExample();
        exOrderExample.createCriteria().andUserIdEqualTo(userId).andOrderTypeEqualTo(1);
        List<ExOrderDto> exOrders = exOrderMapper.selectByExample(exOrderExample);
        if (exOrders.size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * 根据userId查询所有的订单，没有返回false
     *
     * @param userId
     * @return
     */
    public boolean ifExistsOrder(String userId) {
        //根据userId查询所有的订单列表
        ExOrderExample exOrderExample = new ExOrderExample();
        exOrderExample.createCriteria().andUserIdEqualTo(userId);
        List<ExOrderDto> exOrders = exOrderMapper.selectByExample(exOrderExample);
        if (exOrders.size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * 根据userId查询所有的未支付订单，没有返回false
     *
     * @param userId
     * @return
     */
    public boolean ifExistsNoPayOrder(String userId) {
        //根据userId查询所有的订单列表
        ExOrderExample exOrderExample = new ExOrderExample();
        exOrderExample.createCriteria().andUserIdEqualTo(userId).andOrderTypeEqualTo(0);
        List<ExOrderDto> exOrders = exOrderMapper.selectByExample(exOrderExample);
        if (exOrders.size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * 根据userId查询订单中所有已完成的商品列表
     *
     * @param userId
     * @return
     */
    public PageUtils<OrderPayedListVo> queryItemsPerOrder(String userId, Integer pageNo, Integer pageSize) {
        if (pageNo == null) {
            pageNo = 1;
        }
        if (pageSize == null) {
            pageSize = 4;
        }
        PageUtils pageUtils = new PageUtils();
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setPageNo(pageNo);
        ExOrderdetailExample exOrderdetailExample = new ExOrderdetailExample();
        exOrderdetailExample.createCriteria().andUserIdEqualTo(userId).andIfPayEqualTo(1);
        //查询总条数
        long totalItemsCount = exOrderdetailMapper.countByExample(exOrderdetailExample);
        pageUtils.setTotalCount(totalItemsCount);
        //分页查询
        List<ExOrderdetail> exOrderdetails = exOrderdetailMapper.queryExOrderdetails(userId, pageUtils.getPageNo(), pageSize);
        //封装VO
        List<OrderPayedListVo> orderPayedListVos = new ArrayList<OrderPayedListVo>();
        exOrderdetails.forEach(exOrderdetail -> {
            OrderPayedListVo orderPayedListVo = new OrderPayedListVo();
            //根据itemId获取对应的商品信息
            ExItemsExample exItemsExample = new ExItemsExample();
            exItemsExample.createCriteria().andItemIdEqualTo(exOrderdetail.getItemId());
            ExItemsDto exItems = exItemsMapper.selectByExample(exItemsExample).get(0);
            //订单Id
            orderPayedListVo.setOrderId(exOrderdetail.getOrderId());
            //商品名称
            orderPayedListVo.setItemName(exItems.getItemName());
            //商品主图
            orderPayedListVo.setPicMain(exItems.getPicMain());
            //店铺类型
            ExShoptypeExample exShoptypeExample = new ExShoptypeExample();
            exShoptypeExample.createCriteria().andShopTypeEqualTo(exItems.getShopType());
            String shopTypeName = exShoptypeMapper.selectByExample(exShoptypeExample).get(0).getShopTypename();
            orderPayedListVo.setShopType(shopTypeName);
            //商品数量
            orderPayedListVo.setItemCount(exOrderdetail.getItemCount());
            //商品价格
            orderPayedListVo.setItemPrice(exOrderdetail.getItemPrice());
            //商品总价，考虑折扣优惠
            orderPayedListVo.setLastPrice(exOrderdetail.getItemTotalprice());
            //是否已完成支付
            orderPayedListVo.setIfPayed(exOrderdetail.getIfPay() == 1 ? "已完成" : "未支付");
            //收货地址
            orderPayedListVo.setAddress(exOrderdetail.getAddress());
            orderPayedListVos.add(orderPayedListVo);
        });
        pageUtils.setCurrentList(orderPayedListVos);
        return pageUtils;
    }
    /**
     * 根据userId查询订单中所有已完成的商品列表
     *
     * @param userId
     * @return
     */
    public PageUtils<OrderPayedListVo> queryItemsPerOrder2(String userId, Integer pageNo, Integer pageSize) {
        if (pageNo == null) {
            pageNo = 1;
        }
        if (pageSize == null) {
            pageSize = 4;
        }
        PageUtils pageUtils = new PageUtils();
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setPageNo(pageNo);
        ExOrderdetailExample exOrderdetailExample = new ExOrderdetailExample();
        exOrderdetailExample.createCriteria().andUserIdEqualTo(userId).andIfPayEqualTo(1);
        //查询总条数
        long totalItemsCount = exOrderdetailMapper.countByExample(exOrderdetailExample);
        pageUtils.setTotalCount(totalItemsCount);
        //分页查询
        List<ExOrderdetail> exOrderdetails = exOrderdetailMapper.queryExOrderdetails(userId, pageUtils.getPageNo(), pageSize);
        //封装VO
        List<OrderPayedListVo> orderPayedListVos = new ArrayList<OrderPayedListVo>();
        exOrderdetails.forEach(exOrderdetail -> {
            OrderPayedListVo orderPayedListVo = new OrderPayedListVo();
            //根据itemId获取对应的商品信息
            ExItemsExample exItemsExample = new ExItemsExample();
            exItemsExample.createCriteria().andItemIdEqualTo(exOrderdetail.getItemId());
            ExItemsDto exItems = exItemsMapper.selectByExample(exItemsExample).get(0);
            //订单Id
            orderPayedListVo.setOrderId(exOrderdetail.getOrderId());
            //商品名称
            orderPayedListVo.setItemName(exItems.getItemName());
            //商品主图
            orderPayedListVo.setPicMain(exItems.getPicMain());
            //店铺类型
            ExShoptypeExample exShoptypeExample = new ExShoptypeExample();
            exShoptypeExample.createCriteria().andShopTypeEqualTo(exItems.getShopType());
            String shopTypeName = exShoptypeMapper.selectByExample(exShoptypeExample).get(0).getShopTypename();
            orderPayedListVo.setShopType(shopTypeName);
            //商品数量
            orderPayedListVo.setItemCount(exOrderdetail.getItemCount());
            //商品价格
            orderPayedListVo.setItemPrice(exOrderdetail.getItemPrice());
            //商品总价，考虑折扣优惠
            orderPayedListVo.setLastPrice(exOrderdetail.getItemTotalprice());
            //是否已完成支付
            orderPayedListVo.setIfPayed("待收货");
            //收货地址
            orderPayedListVo.setAddress(exOrderdetail.getAddress());
            orderPayedListVos.add(orderPayedListVo);
        });
        pageUtils.setCurrentList(orderPayedListVos);
        return pageUtils;
    }

    /**
     * 根据userId查询所有未支付的订单
     *
     * @param userId
     * @return
     */
    public PageUtils<OrderListVo> queryItemsPerOrderNoPay2(String userId, Integer pageNo, Integer pageSize) {
        if (pageNo == null) {
            pageNo = 1;
        }
        if (pageSize == null) {
            pageSize = 4;
        }
        PageUtils pageUtils = new PageUtils();
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setPageNo(pageNo);
        ExOrderdetailExample exOrderdetailExample = new ExOrderdetailExample();
        exOrderdetailExample.createCriteria().andUserIdEqualTo(userId).andIfPayEqualTo(1);
        //查询总条数
        long totalItemsCount = exOrderdetailMapper.countByExample(exOrderdetailExample);
        pageUtils.setTotalCount(totalItemsCount);
        //分页查询
        List<ExOrderdetail> exOrderdetails = exOrderdetailMapper.queryExOrderdetails2(userId, pageUtils.getPageNo(), pageSize);
        //封装VO
        List<OrderPayedListVo> orderPayedListVos = new ArrayList<OrderPayedListVo>();
        exOrderdetails.forEach(exOrderdetail -> {
            OrderPayedListVo orderPayedListVo = new OrderPayedListVo();
            //根据itemId获取对应的商品信息
            ExItemsExample exItemsExample = new ExItemsExample();
            exItemsExample.createCriteria().andItemIdEqualTo(exOrderdetail.getItemId());
            ExItemsDto exItems = exItemsMapper.selectByExample(exItemsExample).get(0);
            //订单Id
            orderPayedListVo.setOrderId(exOrderdetail.getOrderId());
            //商品名称
            orderPayedListVo.setItemName(exItems.getItemName());
            //商品主图
            orderPayedListVo.setPicMain(exItems.getPicMain());
            //店铺类型
            ExShoptypeExample exShoptypeExample = new ExShoptypeExample();
            exShoptypeExample.createCriteria().andShopTypeEqualTo(exItems.getShopType());
            String shopTypeName = exShoptypeMapper.selectByExample(exShoptypeExample).get(0).getShopTypename();
            orderPayedListVo.setShopType(shopTypeName);
            //商品数量
            orderPayedListVo.setItemCount(exOrderdetail.getItemCount());
            //商品价格
            double price1 = exOrderdetail.getItemPrice();
            orderPayedListVo.setItemPrice((double) Math.round(price1 * 100) / 100);
            //商品总价，考虑折扣优惠
            double price2 = exOrderdetail.getItemPrice();
            orderPayedListVo.setLastPrice((double) Math.round(price2 * 100) / 100);
            //是否已完成支付
            orderPayedListVo.setIfPayed(exOrderdetail.getIfPay() == 1 ? "已完成" : "未支付");
            //收货地址
            orderPayedListVo.setAddress(exOrderdetail.getAddress());
            orderPayedListVos.add(orderPayedListVo);
        });
        pageUtils.setCurrentList(orderPayedListVos);
        return pageUtils;
    }

    /**
     * 根据userId查询所有未支付的订单
     *
     * @param userId
     * @return
     */
    public PageUtils<OrderListVo> queryItemsPerOrderNoPay(String userId, Integer pageNo, Integer pageSize) {
        if (pageNo == null) {
            pageNo = 1;
        }
        if (pageSize == null) {
            pageSize = 4;
        }
        PageUtils pageUtils = new PageUtils();
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setPageNo(pageNo);
        //查询所有未支付订单的条数
        ExOrderExample exOrderExample = new ExOrderExample();
        exOrderExample.createCriteria().andUserIdEqualTo(userId).andOrderTypeEqualTo(0);
        Long total = exOrderMapper.countByExample(exOrderExample);
        pageUtils.setTotalCount(total);
        //分页查询所有未支付订单
        List<ExOrderDto> exOrders = exOrderMapper.queryOrderListNoPay(userId, pageUtils.getPageNo(), pageSize);
        //封装VO
        List<OrderListVo> orderListVos = new ArrayList<OrderListVo>();
        exOrders.forEach(exOrder -> {
            //商品列表集合
            OrderListVo orderListVo = new OrderListVo();
            //订单Id
            orderListVo.setOrderId(exOrder.getOrderId());
            List<OrderPayedListVo> orderPayedListVos = new ArrayList<OrderPayedListVo>();
            //根据orderId查询订单详情
            ExOrderdetailExample exOrderdetailExample1 = new ExOrderdetailExample();
            exOrderdetailExample1.createCriteria().andOrderIdEqualTo(exOrder.getOrderId());
            List<ExOrderdetail> exOrderdetails = exOrderdetailMapper.selectByExample(exOrderdetailExample1);
            double lastOrderPrice = 0.0;
            for (ExOrderdetail exOrderdetail : exOrderdetails) {
                OrderPayedListVo orderPayedListVo = new OrderPayedListVo();
                //根据itemId获取对应的商品信息
                ExItemsExample exItemsExample = new ExItemsExample();
                exItemsExample.createCriteria().andItemIdEqualTo(exOrderdetail.getItemId());
                ExItemsDto exItems = exItemsMapper.selectByExample(exItemsExample).get(0);
                //订单Id
                orderPayedListVo.setOrderId(exOrderdetail.getOrderId());
                //商品名称
                orderPayedListVo.setItemName(exItems.getItemName());
                //商品主图
                orderPayedListVo.setPicMain(exItems.getPicMain());
                //店铺类型
                ExShoptypeExample exShoptypeExample = new ExShoptypeExample();
                exShoptypeExample.createCriteria().andShopTypeEqualTo(exItems.getShopType());
                String shopTypeName = exShoptypeMapper.selectByExample(exShoptypeExample).get(0).getShopTypename();
                orderPayedListVo.setShopType(shopTypeName);
                //商品数量
                orderPayedListVo.setItemCount(exOrderdetail.getItemCount());
                //商品价格
                orderPayedListVo.setItemPrice(exOrderdetail.getItemPrice());
                //商品总价，考虑折扣优惠
                orderPayedListVo.setLastPrice(exOrderdetail.getItemTotalprice());
                lastOrderPrice += exOrderdetail.getItemTotalprice();
                //是否已完成支付
                orderPayedListVo.setIfPayed(exOrderdetail.getIfPay() == 1 ? "已完成" : "未支付");
                //收货地址
                orderPayedListVo.setAddress(exOrderdetail.getAddress());
                orderPayedListVos.add(orderPayedListVo);
            }
            orderListVo.setOrderPayedListVos(orderPayedListVos);
            //店铺类型
            orderListVo.setShopType(orderListVo.getOrderPayedListVos().get(0).getShopType());
            //商品合计价格(考虑折扣优惠)
            orderListVo.setLastPrice(lastOrderPrice);
            //是否完成支付
            orderListVo.setIfPayed("未支付");
            orderListVos.add(orderListVo);
        });
        pageUtils.setCurrentList(orderListVos);
        return pageUtils;
    }

    /**
     * 根据userId查询所有的订单
     *
     * @param userId
     * @return
     */
    public PageUtils<OrderListVo> queryAllItemsPerOrder(String userId, Integer pageNo, Integer pageSize) {
        if (pageNo == null) {
            pageNo = 1;
        }
        if (pageSize == null) {
            pageSize = 4;
        }
        PageUtils pageUtils = new PageUtils();
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setPageNo(pageNo);
        //查询所有订单的条数
        ExOrderExample exOrderExample = new ExOrderExample();
        exOrderExample.createCriteria().andUserIdEqualTo(userId);
        Long total = exOrderMapper.countByExample(exOrderExample);
        pageUtils.setTotalCount(total);
        //分页查询所有订单
        List<ExOrderDto> exOrders = exOrderMapper.queryOrderList(userId, pageUtils.getPageNo(), pageSize);
        //封装VO
        List<OrderListVo> orderListVos = new ArrayList<OrderListVo>();
        exOrders.forEach(exOrder -> {
            String ifPay = exOrder.getOrderType() == 1 ? "已完成" : "未支付";
            //商品列表集合
            OrderListVo orderListVo = new OrderListVo();
            //订单Id
            orderListVo.setOrderId(exOrder.getOrderId());
            List<OrderPayedListVo> orderPayedListVos = new ArrayList<OrderPayedListVo>();
            //根据orderId查询订单详情
            ExOrderdetailExample exOrderdetailExample1 = new ExOrderdetailExample();
            exOrderdetailExample1.createCriteria().andOrderIdEqualTo(exOrder.getOrderId());
            List<ExOrderdetail> exOrderdetails = exOrderdetailMapper.selectByExample(exOrderdetailExample1);
            double lastOrderPrice = 0.0;
            for (ExOrderdetail exOrderdetail : exOrderdetails) {
                OrderPayedListVo orderPayedListVo = new OrderPayedListVo();
                //根据itemId获取对应的商品信息
                ExItemsExample exItemsExample = new ExItemsExample();
                exItemsExample.createCriteria().andItemIdEqualTo(exOrderdetail.getItemId());
                ExItemsDto exItems = exItemsMapper.selectByExample(exItemsExample).get(0);
                //订单Id
                orderPayedListVo.setOrderId(exOrderdetail.getOrderId());
                //商品名称
                orderPayedListVo.setItemName(exItems.getItemName());
                //商品主图
                orderPayedListVo.setPicMain(exItems.getPicMain());
                //店铺类型
                ExShoptypeExample exShoptypeExample = new ExShoptypeExample();
                exShoptypeExample.createCriteria().andShopTypeEqualTo(exItems.getShopType());
                String shopTypeName = exShoptypeMapper.selectByExample(exShoptypeExample).get(0).getShopTypename();
                orderPayedListVo.setShopType(shopTypeName);
                //商品数量
                orderPayedListVo.setItemCount(exOrderdetail.getItemCount());
                //商品价格
                orderPayedListVo.setItemPrice(exOrderdetail.getItemPrice());
                //商品总价，考虑折扣优惠
                orderPayedListVo.setLastPrice(exOrderdetail.getItemTotalprice());
                lastOrderPrice += exOrderdetail.getItemTotalprice();
                //是否已完成支付
                orderPayedListVo.setIfPayed(exOrderdetail.getIfPay() == 1 ? "已完成" : "未支付");
                //收货地址
                orderPayedListVo.setAddress(exOrderdetail.getAddress());
                orderPayedListVos.add(orderPayedListVo);
            }
            orderListVo.setOrderPayedListVos(orderPayedListVos);
            //店铺类型
            orderListVo.setShopType(orderListVo.getOrderPayedListVos().get(0).getShopType());
            //商品合计价格(考虑折扣优惠)
            orderListVo.setLastPrice(lastOrderPrice);
            //是否完成支付
            orderListVo.setIfPayed(ifPay);
            orderListVos.add(orderListVo);
        });
        pageUtils.setCurrentList(orderListVos);
        return pageUtils;
    }
    /**
     * 根据userId查询所有的订单
     *
     * @param userId
     * @return
     */
    public PageUtils<OrderListVo> queryAllItemsPerOrder2(String userId, Integer pageNo, Integer pageSize) {
        if (pageNo == null) {
            pageNo = 1;
        }
        if (pageSize == null) {
            pageSize = 4;
        }
        PageUtils pageUtils = new PageUtils();
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setPageNo(pageNo);
        ExOrderdetailExample exOrderdetailExample = new ExOrderdetailExample();
        exOrderdetailExample.createCriteria().andUserIdEqualTo(userId).andIfPayEqualTo(1);
        //查询总条数
        long totalItemsCount = exOrderdetailMapper.countByExample(exOrderdetailExample);
        pageUtils.setTotalCount(totalItemsCount);
        //分页查询
        List<ExOrderdetail> exOrderdetails = exOrderdetailMapper.queryExOrderdetails3(userId, pageUtils.getPageNo(), pageSize);
        //封装VO
        List<OrderPayedListVo> orderPayedListVos = new ArrayList<OrderPayedListVo>();
        exOrderdetails.forEach(exOrderdetail -> {
            OrderPayedListVo orderPayedListVo = new OrderPayedListVo();
            //根据itemId获取对应的商品信息
            ExItemsExample exItemsExample = new ExItemsExample();
            exItemsExample.createCriteria().andItemIdEqualTo(exOrderdetail.getItemId());
            ExItemsDto exItems = exItemsMapper.selectByExample(exItemsExample).get(0);
            //订单Id
            orderPayedListVo.setOrderId(exOrderdetail.getOrderId());
            //商品名称
            orderPayedListVo.setItemName(exItems.getItemName());
            //商品主图
            orderPayedListVo.setPicMain(exItems.getPicMain());
            //店铺类型
            ExShoptypeExample exShoptypeExample = new ExShoptypeExample();
            exShoptypeExample.createCriteria().andShopTypeEqualTo(exItems.getShopType());
            String shopTypeName = exShoptypeMapper.selectByExample(exShoptypeExample).get(0).getShopTypename();
            orderPayedListVo.setShopType(shopTypeName);
            //商品数量
            orderPayedListVo.setItemCount(exOrderdetail.getItemCount());
            //商品价格
            double price1 = exOrderdetail.getItemPrice();
            orderPayedListVo.setItemPrice((double) Math.round(price1 * 100) / 100);
            //商品总价，考虑折扣优惠
            double price2 = exOrderdetail.getItemPrice();
            orderPayedListVo.setLastPrice((double) Math.round(price2 * 100) / 100);
            //是否已完成支付
            orderPayedListVo.setIfPayed(exOrderdetail.getIfPay() == 1 ? "已完成" : "未支付");
            //收货地址
            orderPayedListVo.setAddress(exOrderdetail.getAddress());
            orderPayedListVos.add(orderPayedListVo);
        });
        pageUtils.setCurrentList(orderPayedListVos);
        return pageUtils;
    }

    /**
     * 删除订单
     *
     * @param orderId
     * @param userId
     * @return
     */
    public boolean deleteOrder(String orderId, String userId) {
        boolean flag = false;
        ExOrderExample exOrderExample = new ExOrderExample();
        exOrderExample.createCriteria().andOrderIdEqualTo(orderId).andUserIdEqualTo(userId);
        ExOrderdetailExample exOrderdetailExample = new ExOrderdetailExample();
        exOrderdetailExample.createCriteria().andOrderIdEqualTo(orderId).andUserIdEqualTo(userId);
        try {
            flag = exOrderMapper.deleteByExample(exOrderExample) > 0;
        } catch (Exception e) {
            log.error("删除订单失败！" + e);
            e.printStackTrace();
        }
        try {
            flag = exOrderdetailMapper.deleteByExample(exOrderdetailExample) > 0;
        } catch (Exception e) {
            log.error("删除订单失败！" + e);
            e.printStackTrace();
        }
        return flag;
    }

    public void createOrder(String address, String userId, String... itemIds) {
        //生成统一共用订单ID
        String orderId = UuidUtils.getTimeUUID();
        double totalPrice = 0;
        for (String itemId : itemIds) {
            ExCartdetailDto exCartdetailDto = exCartdetailMapper.selectByItemId(itemId, userId);
            ExOrderdetail exOrderdetailDto = new ExOrderdetail();
            BeanUtils.copyProperties(exCartdetailDto, exOrderdetailDto);
            userId = exOrderdetailDto.getUserId();
            double price = itemService.queryGoods(itemId).getItemPrice();
            int itemCount = exOrderdetailDto.getItemCount();
            double itemTotalPrice = price * itemCount;
            totalPrice += itemTotalPrice;
            exOrderdetailDto.setOrderId(orderId);
            exOrderdetailDto.setItemPrice(price);
            exOrderdetailDto.setItemTotalprice(itemTotalPrice);
            exOrderdetailDto.setIfPay(0);
            exOrderdetailDto.setAddress(address);
            exOrderdetailMapper.insertSelective(exOrderdetailDto);
            //删除购物车中itemId对应信息
            exCartdetailMapper.deleteByItemId(itemId);
        }
        ExOrderDto exOrderDto = new ExOrderDto();
        exOrderDto.setOrderId(orderId);
        exOrderDto.setUserId(userId);
        exOrderDto.setTotalPrice(totalPrice);
        exOrderDto.setOrderType(0);
        exOrderMapper.insertSelective(exOrderDto);
    }

    public boolean ifExistUser(String userId) {
        ExUserExample exUserExample = new ExUserExample();
        exUserExample.createCriteria().andUserIdEqualTo(userId);
        if (exUserMapper.countByExample(exUserExample) > 0) {
            return true;
        }
        return false;
    }


}
