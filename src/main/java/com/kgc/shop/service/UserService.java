package com.kgc.shop.service;

import com.kgc.shop.cons.UserParam;
import com.kgc.shop.dto.*;
import com.kgc.shop.mapper.ExOrderMapper;
import com.kgc.shop.mapper.ExUserMapper;
import com.kgc.shop.mapper.ExUsercouponMapper;
import com.kgc.shop.vo.UserVo;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author jialin
 * @createTime 2020-09-23
 */
@Log4j
@Service
public class UserService {

    @Autowired
    private ExUserMapper exUserMapper;

    @Autowired
    private ExUsercouponMapper exUsercouponMapper;

    @Autowired
    private ExOrderMapper exOrderMapper;

    /**
     * 根据userId查询用户个人信息
     *
     * @param userId
     * @return
     */
    public UserVo queryUser(String userId) {
        ExUserDto exUserDto = exUserMapper.selectByPrimaryKey(userId);
        UserVo userVo = new UserVo();
        BeanUtils.copyProperties(exUserDto, userVo);
        //待支付数
        ExOrderExample exOrderExample = new ExOrderExample();
        exOrderExample.createCriteria().andUserIdEqualTo(userId);
        List<ExOrderDto> exOrderDtos = exOrderMapper.selectByExample(exOrderExample);
        int count = exOrderDtos.size();
        userVo.setToPayCount(count);
        //优惠券数
        ExUsercouponExample exUsercouponExample = new ExUsercouponExample();
        exUsercouponExample.createCriteria().andUserIdEqualTo(userId);
        List<ExUsercouponDto> exUsercoupons = exUsercouponMapper.selectByExample(exUsercouponExample);
        int cCount = exUsercoupons.size();
        userVo.setCouponCount(cCount);
        return userVo;
    }

    @Async
    public void insertUser(UserParam userParam) {
        ExUserDto exUserDto = new ExUserDto();
        BeanUtils.copyProperties(userParam, exUserDto);
        exUserMapper.insertSelective(exUserDto);
    }

    public void modUser(ExUserDto exUserDto){
        exUserMapper.updateByPrimaryKeySelective(exUserDto);
    }

}
