package com.kgc.shop.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author jialin
 */
@Configuration
@MapperScan(basePackages = "com.kgc.shop.mapper")
public class MybatisConfig {
}
