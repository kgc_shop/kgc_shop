package com.kgc.shop.params;

import lombok.Data;

import java.io.Serializable;

/**
 * @author jialin
 * @createTime 2020-10-12
 */
@Data
public class OutletDetail implements Serializable {
    private OutletInfo outletInfo;
    private String distance;

    public OutletDetail() {
    }

    public OutletDetail(OutletInfo outletInfo, String distance) {
        this.outletInfo = outletInfo;
        this.distance = distance;
    }
}
