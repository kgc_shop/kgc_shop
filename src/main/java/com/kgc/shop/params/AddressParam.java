package com.kgc.shop.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by XuHui on 2020/9/25
 */
@Data
@ApiModel("地址表")
public class AddressParam {
    @ApiModelProperty("当前用户ID")
    private String userId;

    @ApiModelProperty("收货人姓名")
    private String receiverName;

    @ApiModelProperty("收货人手机")
    private String receiverPhone;

    @ApiModelProperty("收货人地址")
    private String receiverAddress;

    @ApiModelProperty("邮政编码")
    private String postalCode;
}
