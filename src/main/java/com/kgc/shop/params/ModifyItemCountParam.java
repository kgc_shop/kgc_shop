package com.kgc.shop.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

/**
 * @author ：jacketzc
 * @date ：Created in 2020/9/23 19:15
 */
@Data
@ApiModel("修改购物车中商品数量入参模型")
public class ModifyItemCountParam {
    @ApiModelProperty("需要修改数量的商品id")
    private String itemId;
    @ApiModelProperty("修改到的数量：应该发送最终修改完成的数量，而不是+1 -1这种")
    private Integer itemCount;
}
