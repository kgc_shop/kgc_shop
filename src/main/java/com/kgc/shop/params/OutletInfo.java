package com.kgc.shop.params;

import java.io.Serializable;

/**
 * @author jialin
 * @createTime 2020-10-12
 */
public class OutletInfo implements Serializable {
    /**
     * 网点名称
     * 网点位置
     * 网点电话
     * 距离您 x,y
     * 网点地图
     */
    private String title;
    private String address;
    private String phone;
    private double x;
    private double y;

    public OutletInfo() {
    }

    public OutletInfo(String title, String address, String phone, double x, double y) {
        this.title = title;
        this.address = address;
        this.phone = phone;
        this.x = x;
        this.y = y;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "OutletInfo{" +
                "title='" + title + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
