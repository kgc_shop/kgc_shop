package com.kgc.shop.cons;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jialin
 * @createTime 2020-09-25
 */
@Data
public class JsCode {
    @ApiModelProperty("前端传的jscode")
    private String  jscode;
}
