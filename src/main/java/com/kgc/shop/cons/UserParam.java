package com.kgc.shop.cons;

import lombok.Data;

/**
 * @author jialin
 * @createTime 2020-09-23
 */
@Data
public class UserParam {
    
    private String userId;
    private String userPic;
    private String userName;
    private String userPhone;
    private String userAddress;
    
}
