package com.kgc.shop.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author ：jacketzc
 * @date ：Created in 2020/9/22 9:52
 */
@Data
@ApiModel("微信登录后返回的用户信息")
public class WxUserVo {
    @ApiModelProperty("用户的唯一标识")
    private String openid;
    @ApiModelProperty("用户昵称")
    private String nickname;
    @ApiModelProperty("用户的性别，值为1时是男性，值为2时是女性，值为0时是未知")
    private Integer sex;
    @ApiModelProperty("用户个人资料填写的省份")
    private String province;
    @ApiModelProperty("普通用户个人资料填写的城市")
    private String city;
    @ApiModelProperty("国家，如中国为CN")
    private String country;
    @ApiModelProperty("用户头像url")
    private String headimgurl;
}
