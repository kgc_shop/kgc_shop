package com.kgc.shop.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table receiver_info
 *
 * @mbg.generated do_not_delete_during_merge
 */
@Data
public class ReceiverInfoVo {
    @ApiModelProperty("地址信息ID")
    private Integer id;

    @ApiModelProperty("当前用户ID")
    private String userId;

    @ApiModelProperty("收货人姓名")
    private String receiverName;

    @ApiModelProperty("收货人手机")
    private String receiverPhone;

    @ApiModelProperty("收货详细地址")
    private String receiverAddress;

    @ApiModelProperty("邮政编码")
    private String postalCode;

}