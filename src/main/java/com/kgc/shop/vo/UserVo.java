package com.kgc.shop.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jialin
 * @createTime 2020-09-23
 */
@Data
@ApiModel("用户个人中心VO")
public class UserVo {

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("用户手机")
    private String userPhone;

    @ApiModelProperty("用户地址")
    private String userAddress;

    @ApiModelProperty("用户头像")
    private String userPic;

    @ApiModelProperty("待支付数")
    private Integer toPayCount;

    @ApiModelProperty("优惠券数")
    private Integer couponCount;
    
}
