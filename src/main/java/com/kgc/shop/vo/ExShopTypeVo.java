package com.kgc.shop.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jialin
 * @createTime 2020-09-23
 */
@Data
@ApiModel("店铺类型VO")
public class ExShopTypeVo {

    @ApiModelProperty("店铺类型id")
    private Integer shopType;

    @ApiModelProperty("店铺类型名称")
    private String shopTypeName;

}
