package com.kgc.shop.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jialin
 * @createTime 2020-09-23
 */
@Data
@ApiModel("首页顶部商品分类VO")
public class ExItemTypeVo {
    
    @ApiModelProperty("商品分类id")
    private Integer itemType;

    @ApiModelProperty("商品分类名称")
    private String typeName;

}
