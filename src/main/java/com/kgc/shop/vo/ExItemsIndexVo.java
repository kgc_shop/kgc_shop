package com.kgc.shop.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jialin
 * @createTime 2020-09-23
 */
@Data
@ApiModel("商品首页列表VO")
public class ExItemsIndexVo {

    @ApiModelProperty("商品id")
    private String itemId;

    @ApiModelProperty("商品名称")
    private String itemName;

    @ApiModelProperty("商品主图，列表上展示的图片")
    private String picMain;

    @ApiModelProperty("商品原价，有些商品原价和折扣价一样。")
    private Double itemPrice;

    @ApiModelProperty("商品折扣价")
    private Double discountPrice;

    @ApiModelProperty("商品类型")
    private String itemTypeName;

    @ApiModelProperty("店铺类型")
    private String shopTypeName;

    @ApiModelProperty("评价数")
    private Long commentCount;

    @ApiModelProperty("商品描述")
    private String itemDetail;


}
