package com.kgc.shop.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("商品类型VO")
public class ExItemIndextypeVo {

    @ApiModelProperty("商品类型编号")
    private Integer itemType;

    @ApiModelProperty("商品类型名称")
    private String typeName;
}