package com.kgc.shop.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table comment
 *
 * @mbg.generated do_not_delete_during_merge
 */
@Data
@ApiModel("评论表")
public class CommentVo {
    /**
     * Database Column Remarks:
     *   用户id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column comment.user_id
     *
     * @mbg.generated
     */
    @ApiModelProperty("用户id")
    private String userId;

    /**
     *  用户名
     */
    @ApiModelProperty("用户名")
    private String userName;

    /**
     * Database Column Remarks:
     *   评论
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column comment.comment
     *
     * @mbg.generated
     */
    @ApiModelProperty("评论")
    private String comment;

    /**
     * Database Column Remarks:
     *   评论图片
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column comment.pic_comment
     *
     * @mbg.generated
     */
    @ApiModelProperty("评论图片")
    private String picComment;

    /**
     * Database Column Remarks:
     *   商品id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column comment.item_id
     *
     * @mbg.generated
     */
    @ApiModelProperty("商品id")
    private String itemId;

    /**
     * Database Column Remarks:
     *   评价星级
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column comment.evaluate
     *
     * @mbg.generated
     */
    @ApiModelProperty("评价星级")
    private Integer evaluate;

    /**
     * Database Column Remarks:
     *   评论时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column comment.createTime
     *
     * @mbg.generated
     */
    @ApiModelProperty("评论时间")
    private String createTime;

    @ApiModelProperty("用户头像")
    private String userPic;


}