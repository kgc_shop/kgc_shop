package com.kgc.shop.controller;

import com.kgc.shop.params.OutletDetail;
import com.kgc.shop.params.OutletInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.*;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.connection.RedisGeoCommands.GeoLocation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jialin
 * @createTime 2020-10-12
 */
@RestController
@RequestMapping(value = "/geo")
public class GeoController {

    @Autowired
    private RedisTemplate redisTemplate;

    /*
     *基于前端传递的坐标和指定的距离，在网点集合中进行范围查找
     * 返回网点基本信息，以及用户距离当前网点的距离
     */
    @GetMapping(value = "/outlets")
    public List<OutletDetail> outlets(@RequestParam String pos, @RequestParam double dis) {
        //1.基于pos和dis进行范围查找
        //构建距离和中心点对象 pos格式 118.906586,31.920188
        String[] split = pos.split(",");
        Point point = new Point(new Double(split[0]), new Double(split[1]));
        Distance distance = new Distance(dis, Metrics.KILOMETERS);
        Circle circle = new Circle(point, distance);
        //指定返回结果中包含距离信息
        RedisGeoCommands.GeoRadiusCommandArgs args = RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs();
        args.includeDistance();
        GeoResults<GeoLocation<String>> results = redisTemplate.boundGeoOps("outlets").geoRadius(circle, args);
        //封装返回结果
        List<OutletDetail> list = new ArrayList<>();
        for (GeoResult<GeoLocation<String>> result : results) {
            //result 距离信息
            Distance dis2 = result.getDistance();
            String unit = "km".equals(dis2.getUnit()) ? "公里" : "km";
            //距离
            String val = new BigDecimal(dis2.getValue()).setScale(2, BigDecimal.ROUND_HALF_UP) + unit;
            //网点基本信息
             GeoLocation<String> location = result.getContent();
             //网点名称
            String name = location.getName();
            //获取网点基本信息
            OutletInfo outletInfo = (OutletInfo) redisTemplate.boundHashOps("outletsInfo").get(name);
            //构建outletDetail对象
            OutletDetail outletDetail = new OutletDetail(outletInfo, val);
            //放入集合
            list.add(outletDetail);
        }
        return list;
    }

}
