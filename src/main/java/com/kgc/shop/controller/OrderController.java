package com.kgc.shop.controller;

import com.kgc.shop.config.api.CurrentUser;
import com.kgc.shop.config.api.LoginRequired;
import com.kgc.shop.service.OrderService;
import com.kgc.shop.utils.PageUtils;
import com.kgc.shop.utils.ReturnResult;
import com.kgc.shop.utils.ReturnResultUtils;
import com.kgc.shop.vo.OrderListVo;
import com.kgc.shop.vo.OrderPayedListVo;
import com.kgc.shop.vo.WxUserVo;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author jialin
 * @createTime 2020-09-21
 */
@Api(tags = "订单列表页接口")
@RestController
@RequestMapping(value = "/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @LoginRequired
    @ApiOperation(value = "查询已完成的订单", notes = "查询已完成")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 501, message = "没有相关数据")
    })
    @GetMapping(value = "/queryAllPayed")
    public ReturnResult<OrderPayedListVo> queryAllPayed(@ApiParam(value = "第几页") @RequestParam(required = false) Integer pageNo, @ApiParam(value = "查几条") @RequestParam(required = false) Integer pageSize, @ApiParam(value = "登录后的用户对象") @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        if (!orderService.ifPay(userId)) {
            return ReturnResultUtils.returnFailMsg(501, "没有相关数据");
        }
        PageUtils<OrderPayedListVo> orderPayedListVoPageUtils = orderService.queryItemsPerOrder(userId, pageNo, pageSize);
        return ReturnResultUtils.returnSuccess(orderPayedListVoPageUtils);
    }

    @LoginRequired
    @ApiOperation(value = "查询待收货的订单", notes = "查询已完成")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 501, message = "没有相关数据")
    })
    @GetMapping(value = "/queryAllRGet")
    public ReturnResult<OrderPayedListVo> queryAllRGet(@ApiParam(value = "第几页") @RequestParam(required = false) Integer pageNo, @ApiParam(value = "查几条") @RequestParam(required = false) Integer pageSize, @ApiParam(value = "登录后的用户对象") @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        if (!orderService.ifPay(userId)) {
            return ReturnResultUtils.returnFailMsg(501, "没有相关数据");
        }
        PageUtils<OrderPayedListVo> orderPayedListVoPageUtils = orderService.queryItemsPerOrder2(userId, pageNo, pageSize);
        return ReturnResultUtils.returnSuccess(orderPayedListVoPageUtils);
    }

    @LoginRequired
    @ApiOperation(value = "查询所有的订单", notes = "查询所有订单")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 501, message = "没有相关数据")
    })
    @GetMapping(value = "/queryAllOrder")
    public ReturnResult<OrderListVo> queryAllOrder(@ApiParam(value = "第几页") @RequestParam(required = false) Integer pageNo, @ApiParam(value = "查几条") @RequestParam(required = false) Integer pageSize, @ApiParam(value = "登录后的用户对象") @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        if (!orderService.ifPay(userId)) {
            return ReturnResultUtils.returnFailMsg(501, "没有相关数据");
        }
        PageUtils<OrderListVo> orderListVoPageUtils = orderService.queryAllItemsPerOrder2(userId, pageNo, pageSize);
        return ReturnResultUtils.returnSuccess(orderListVoPageUtils);
    }

    @LoginRequired
    @ApiOperation(value = "查询所有的未支付订单", notes = "查询所有未支付订单")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 501, message = "没有相关数据")
    })
    @GetMapping(value = "/queryAllNoPayOrder")
    public ReturnResult<OrderListVo> queryAllNoPayOrder(@ApiParam(value = "第几页") @RequestParam(required = false) Integer pageNo, @ApiParam(value = "查几条") @RequestParam(required = false) Integer pageSize, @ApiParam(value = "登录后的用户对象") @ApiIgnore  @CurrentUser String openid) {
        String userId = openid;
        if (!orderService.ifExistsNoPayOrder(userId)) {
            return ReturnResultUtils.returnFailMsg(501, "没有相关数据");
        }
        PageUtils<OrderListVo> orderNoPayListVoPageUtils = orderService.queryItemsPerOrderNoPay2(userId, pageNo, pageSize);
        return ReturnResultUtils.returnSuccess(orderNoPayListVoPageUtils);
    }

    @ApiOperation(value = "删除订单", notes = "删除订单")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 502, message = "删除订单异常")
    })
    @GetMapping(value = "/delOrder")
    public ReturnResult delOrder(@ApiParam(value = "订单编号") @RequestParam String orderId, @ApiParam(value = "登录后的用户对象") @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        if (!orderService.deleteOrder(orderId, userId)) {
            return ReturnResultUtils.returnFailMsg(502, "删除订单失败！");
        }
        return ReturnResultUtils.returnSuccess("删除成功！");
    }

}
