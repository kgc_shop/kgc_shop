package com.kgc.shop.controller;

import com.kgc.shop.config.api.CurrentUser;
import com.kgc.shop.config.api.LoginRequired;
import com.kgc.shop.service.*;
import com.kgc.shop.utils.PageUtils;
import com.kgc.shop.utils.ReturnResult;
import com.kgc.shop.utils.ReturnResultUtils;
import com.kgc.shop.vo.*;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by XuHui on 2020/9/21
 */
@Api(tags = "商品详情页")
@Log4j
@RestController
@RequestMapping(value = "/GoodsDetail")
public class GoodsDetailController {
    @Autowired
    private ItemService itemService;
    @Autowired
    private StockService stockService;
    @Autowired
    private ExUserService exUserService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private OrderService orderService;

    @ApiOperation(value = "查询商品详情", notes = "查询商品详情")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 405, message = "查询商品异常"),
    })
    @GetMapping(value = "/getGoods")
    public ReturnResult<ExItemsVo> getGoods(@ApiParam(value = "商品ID", required = true) @RequestParam String itemId) {
        try {
            ExItemsVo exItemsVo = itemService.queryGoods(itemId);
            return ReturnResultUtils.returnSuccess(exItemsVo);
        } catch (Exception e) {
            log.error("查询商品出现异常：" + e);
        }
        return ReturnResultUtils.returnFailMsg(405, "查询商品异常");
    }

    @ApiOperation(value = "查询商品可用优惠券", notes = "查询商品可用优惠券")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 406, message = "查询优惠券异常"),
    })
    @GetMapping(value = "/getCoupon")
    public ReturnResult<ExCouponVo> getCoupon(@ApiParam(value = "商品ID", required = true) @RequestParam String itemId) {
        try {
            List<ExCouponVo> exCouponVos = itemService.getCoupon(itemId);
            if (exCouponVos.size() == 0) {
                return ReturnResultUtils.returnSuccess("无可用优惠券！");
            }
            return ReturnResultUtils.returnSuccess(exCouponVos);
        } catch (Exception e) {
            log.error("查询优惠券出现异常：" + e);
        }
        return ReturnResultUtils.returnFailMsg(406, "查询优惠券异常");
    }

    @ApiOperation(value = "查询商品库存详情", notes = "查询商品库存详情")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 407, message = "查询库存异常"),
    })
    @GetMapping(value = "/getStock")
    public ReturnResult<ExItemRepoVo> getStock(@ApiParam(value = "商品ID", required = true) @RequestParam String itemId) {
        try {
            ExItemRepoVo exItemRepoVo = stockService.getStock(itemId);
            return ReturnResultUtils.returnSuccess(exItemRepoVo);
        } catch (Exception e) {
            log.error("查询库存出现异常：" + e);
        }
        return ReturnResultUtils.returnFailMsg(407, "查询库存异常");
    }

    @ApiOperation(value = "查询当前用户详情", notes = "查询当前用户详情")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 408, message = "查询用户异常"),
    })
    @GetMapping(value = "/getUser")
    public ReturnResult<ExUserVo> getUser(@ApiParam(value = "当前用户ID", required = true) @RequestParam String userId) {
        try {
            ExUserVo exUserVo = exUserService.getUser(userId);
            return ReturnResultUtils.returnSuccess(exUserVo);
        } catch (Exception e) {
            log.error("查询用户出现异常：" + e);
        }
        return ReturnResultUtils.returnFailMsg(408, "查询用户异常");
    }

    @ApiOperation(value = "查询评论详情", notes = "查询评论详情")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 409, message = "查询评论异常"),
    })
    @GetMapping(value = "/getComment")
    public ReturnResult<CommentVo> getComment(@ApiParam(value = "第几页") @RequestParam(required = false) Integer pageNo, @ApiParam(value = "查几条") @RequestParam(required = false) Integer pageSize,
                                              @ApiParam(value = "商品ID", required = true) @RequestParam String itemId) {
        try {
            PageUtils<CommentVo> commentVos = commentService.getComment(pageNo, pageSize, itemId);
            return ReturnResultUtils.returnSuccess(commentVos);
        } catch (Exception e) {
            log.error("查询评论出现异常：" + e);
            return ReturnResultUtils.returnFailMsg(409, "查询评论异常");
        }
    }

    @LoginRequired
    @ApiOperation(value = "添加用户优惠券", notes = "添加用户拥有优惠券")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 410, message = "添加用户优惠券异常"),
    })
    @PostMapping(value = "/addCoupon")
    public ReturnResult addCoupon(@ApiParam(value = "优惠券ID", required = true) @RequestParam String couponId,
                                  @ApiParam(value = "登录后的用户对象") @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        try {
            exUserService.addCoupon(couponId, userId);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            log.error("添加用户优惠券出现异常：" + e);
            return ReturnResultUtils.returnFailMsg(410, "添加用户优惠券");
        }
    }

    /*@LoginRequired
    @ApiOperation(value = "查询用户的收货地址", notes = "查询用户的收货地址")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 411, message = "添加用户的收货地址异常"),
    })
    @GetMapping(value = "/queryAddress")
    public ReturnResult queryAddress(@ApiParam(value = "登录后的用户对象") @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        try {
            List<String> address = exUserService.queryAddress(userId);
            //默认是微信用户所在城市
            if (address.size() == 0) {
                Map<String, String> map = new HashMap<>();
                map.put(openid, "暂无数据");
                return ReturnResultUtils.returnSuccess(map);
            }
            Map<String, List<String>> map = new HashMap<>();
            map.put(openid, address);
            return ReturnResultUtils.returnSuccess(map);
        } catch (Exception e) {
            log.error("查询用户的收货地址异常：" + e);
            return ReturnResultUtils.returnFailMsg(411, "查询用户的收货地址异常");
        }
    }*/

    @LoginRequired
    @ApiOperation(value = "生成未支付订单", notes = "生成未支付订单，要先调用查询收货地址的接口，传一个收货地址")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 411, message = "生成未支付订单异常"),
    })
    @PostMapping(value = "/createOrder")
    public ReturnResult createOrder(@ApiParam(value = "收货地址") @RequestParam String address, @ApiIgnore @CurrentUser String openid, @ApiParam(value = "购物车中所有商品ID") @RequestParam String... itemIds) {
        String userId = openid;
        try {
            orderService.createOrder(address, userId, itemIds);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            log.error("生成未支付订单发生异常：" + e);
        }
        return ReturnResultUtils.returnFailMsg(411, "生成未支付订单异常");
    }

}
