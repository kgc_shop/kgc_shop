package com.kgc.shop.controller;

import com.kgc.shop.config.api.CurrentUser;
import com.kgc.shop.config.api.LoginRequired;
import com.kgc.shop.dto.ExUserDto;
import com.kgc.shop.params.AddressParam;
import com.kgc.shop.service.AddressService;
import com.kgc.shop.service.UserService;
import com.kgc.shop.utils.ReturnResult;
import com.kgc.shop.utils.ReturnResultUtils;
import com.kgc.shop.vo.ReceiverInfoVo;
import com.kgc.shop.vo.UserVo;
import com.kgc.shop.vo.WxUserVo;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jialin
 * @createTime 2020-09-23
 */
@Log4j
@Api(tags = "用户个人中心")
@RestController
@RequestMapping(value = "/user")
public class UserController {

    private String  url;

    @Autowired
    private UserService userService;

    @Autowired
    private AddressService addressService;

    @LoginRequired
    @ApiOperation(value = "查询用户个人信息", notes = "用户信息")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 505, message = "查询失败")
    })
    @GetMapping(value = "/queryUser")
    public ReturnResult<UserVo> queryUser(@ApiParam(value = "登录后的用户对象") @ApiIgnore @CurrentUser String openid) {
        try {
            UserVo userVo = userService.queryUser(openid);
            return ReturnResultUtils.returnSuccess(userVo);
        } catch (Exception e) {
            log.error("查询用户个人信息失败！" + e);
            return ReturnResultUtils.returnFailMsg(505, "查询失败！");
        }
    }

    @LoginRequired
    @ApiOperation(value = "修改用户个人信息", notes = "修改user")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 507, message = "修改失败")
    })
    @PostMapping(value = "/modUser")
    public ReturnResult modUser(@ApiParam(value = "用户对象") @RequestBody ExUserDto exUserDto) {
        try {
            System.out.println(123);
            userService.modUser(exUserDto);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            log.error("修改失败！" + e);
            return ReturnResultUtils.returnFailMsg(507, "修改失败！");
        }
    }

    @ApiOperation(value = "用户头像上传", notes = "上传头像")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 509, message = "上传图片不能为空"),
            @ApiResponse(code = 510, message = "文件已存在"),
            @ApiResponse(code = 508, message = "上传头像失败")
    })
    @PostMapping(value = "/userPicUpload")
    public ReturnResult userPicUpload(@RequestParam("uploadFile") MultipartFile multipartFile, HttpServletRequest request) {

        if (multipartFile.isEmpty()) {
            ReturnResultUtils.returnFailMsg(509, "上传图片不能为空！");
        }
        //获取上传文件的名称
        String uploadFileName = multipartFile.getOriginalFilename();
        String path = File.separator + "usr" + File.separator + "upload" + File.separator + "headimages" + File.separator + uploadFileName;
        //创建文件路径
        File dest = new File(path);
        //判断文件是否已经存在
        if (dest.exists()) {
            return ReturnResultUtils.returnFailMsg(510, "文件已经存在");
        }
        //判断文件父目录是否存在
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdir();
        }
        try {
            //上传文件
            multipartFile.transferTo(dest); //保存文件
            System.out.print("保存文件路径" + path + "\n");
        } catch (IOException e) {
            log.error("头像上传失败！" + e);
            ReturnResultUtils.returnFailMsg(508, "上传头像失败");
        }
        String basePath = request.getScheme() + "://" + request.getServerName() + ":"
                + "8848";
        String userPics = basePath + "/usr/upload/headimages/" + uploadFileName;
        Map<String, String> userPicMap = new HashMap<String, String>();
        userPicMap.put("userPic", userPics);
        return ReturnResultUtils.returnSuccess(userPicMap);
    }

    @LoginRequired
    @ApiOperation(value = "查询用户收货地址", notes = "查询用户收货地址")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 413, message = "查询用户收货地址异常"),
    })
    @PostMapping(value = "/queryAllAddress")
    public ReturnResult<ReceiverInfoVo> queryAllAddress(@ApiParam(value = "当前用户ID") @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        try {
            List<ReceiverInfoVo> receiverInfoVos = addressService.queryAllAddress(userId);
            return ReturnResultUtils.returnSuccess(receiverInfoVos);
        } catch (Exception e) {
            log.error("查询用户收货地址出现异常：" + e);
        }
        return ReturnResultUtils.returnFailMsg(413, "查询用户收货地址异常");
    }

    @LoginRequired
    @ApiOperation(value = "查询收货地址详细信息", notes = "查询收货地址详细信息")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 414, message = "查询收货地址详细信息异常"),
    })
    @PostMapping(value = "/queryAddrDetail")
    public ReturnResult<ReceiverInfoVo> queryAddrDetail(@ApiParam(value = "地址信息ID") @RequestParam Integer id, @ApiIgnore @CurrentUser String openid) {
        try {
            ReceiverInfoVo receiverInfoVo = addressService.queryAddrDetail(id);
            return ReturnResultUtils.returnSuccess(receiverInfoVo);
        } catch (Exception e) {
            log.error("查询收货地址详细信息出现异常：" + e);
        }
        return ReturnResultUtils.returnFailMsg(414, "查询收货地址详细信息异常");
    }

    @LoginRequired
    @ApiOperation(value = "新增用户收货地址", notes = "新增用户收货地址")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 413, message = "新增用户收货地址异常"),
    })
    @PostMapping(value = "/addAddress")
    public ReturnResult addAddress(@ApiParam(value = "地址添加信息") @RequestBody AddressParam addressParam, @ApiIgnore @CurrentUser String openid) {
        try {
            addressService.addAddress(addressParam);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            log.error("新增用户收货地址出现异常：" + e);
        }
        return ReturnResultUtils.returnFailMsg(413, "新增用户收货地址异常");
    }

    @LoginRequired
    @ApiOperation(value = "修改收货地址", notes = "修改收货地址")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 414, message = "修改收货地址异常"),
    })
    @PostMapping(value = "/modAddress")
    public ReturnResult modAddress(@ApiParam(value = "地址修改信息") @RequestBody ReceiverInfoVo receiverInfoVo, @ApiIgnore @CurrentUser String openid) {
        try {
            addressService.modAddress(receiverInfoVo);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            log.error("修改收货地址异常: " + e);
        }
        return ReturnResultUtils.returnFailMsg(414, "修改收货地址异常");
    }

    @LoginRequired
    @ApiOperation(value = "删除收货地址", notes = "删除收货地址")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 415, message = "删除收货地址异常"),
    })
    @PostMapping(value = "/delAddress")
    public ReturnResult delAddress(@ApiParam(value = "地址信息ID") @RequestParam Integer id, @ApiIgnore @CurrentUser String openid) {
        try {
            addressService.delAddress(id);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            log.error("删除收货地址出现异常: " + e);
        }
        return ReturnResultUtils.returnFailMsg(415, "删除收货地址异常");
    }
}
