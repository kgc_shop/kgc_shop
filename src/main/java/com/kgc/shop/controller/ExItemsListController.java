package com.kgc.shop.controller;

import com.kgc.shop.service.ExItemsListService;
import com.kgc.shop.utils.PageUtils;
import com.kgc.shop.utils.ReturnResult;
import com.kgc.shop.utils.ReturnResultUtils;
import com.kgc.shop.vo.ExItemIndextypeVo;
import com.kgc.shop.vo.ExItemsIndexVo;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author jialin
 * @createTime 2020-09-23
 */
@Log4j
@Api(tags = "首页商品列表")
@RestController
@RequestMapping(value = "/exItems")
public class ExItemsListController {
    @Autowired
    private ExItemsListService exItemsListService;

    @ApiOperation(value = "商品查询", notes = "参数可选，可以搜索名称，类型，可以加分页")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 506, message = "查询出错")
    })
    @GetMapping(value = "/list")
    public ReturnResult<ExItemsIndexVo> select(@ApiParam(value = "商品名称") @RequestParam(required = false) String itemName, @ApiParam(value = "商品类型") @RequestParam(required = false) Integer itemType,
                                               @ApiParam(value = "第几页") @RequestParam(required = false) Integer pageNo, @ApiParam(value = "查几条") @RequestParam(required = false) Integer pageSize) {
        try {
            PageUtils<ExItemsIndexVo> pageUtils = exItemsListService.select(itemName, itemType, pageNo, pageSize);
            return ReturnResultUtils.returnSuccess(pageUtils);
        } catch (Exception e) {
            log.error("查询出错！ " + e);
            return ReturnResultUtils.returnFailMsg(506, "查询出错！");
        }
    }

    @ApiOperation(value = "商品类型", notes = "商品名查询，输入商品名中的关键字即可进行模糊查询。商品类型查询：1表示精选、2表示食品、3表示母婴、4表示个护清洁、5表示厨卫、6表示小家电、7表示服饰、8表示苏宁健康、9表示鞋靴、10表示钟表眼镜、11表示饰品")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 501, message = "没有相关数据")
    })
    @PostMapping(value = "/type")
    public List<ExItemIndextypeVo> selectType() {
        List<ExItemIndextypeVo> exItemtypeVos = exItemsListService.selectType();
        return exItemtypeVos;
    }

}