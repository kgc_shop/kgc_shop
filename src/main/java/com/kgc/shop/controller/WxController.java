package com.kgc.shop.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.kgc.shop.config.wxconfig.WxConfigure;
import com.kgc.shop.cons.JsCode;
import com.kgc.shop.cons.UserParam;
import com.kgc.shop.service.OrderService;
import com.kgc.shop.service.UserService;
import com.kgc.shop.utils.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ：jacketzc
 * @date ：Created in 2020/9/22 8:46
 */
@Api(tags = "微信操作相关")
@Slf4j
@Controller
@RequestMapping("/wx")
public class WxController {
    @Resource
    private RedisUtils redisUtils;
    @Resource
    private WxConfigure wxConfigure;
    @Autowired
    private UserService userService;
    @Autowired
    private OrderService orderService;


    @ApiOperation("通过code拿用户信息")
    @PostMapping("/getWxUserOpenId")
    @ResponseBody
    public String getWxUserOpenId(@ApiParam("wxLogin返回的code") @RequestBody JsCode jscode) throws IOException {
        String openIdurl = wxConfigure.getOpenId(jscode.getJscode());

        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpGet get = new HttpGet(openIdurl);
        CloseableHttpResponse response = client.execute(get);
        HttpEntity result = response.getEntity();
        String content = EntityUtils.toString(result);
        JSONObject jsonObject = JSONObject.parseObject(content);
        String openid = jsonObject.getString("openid");
        String session_key = jsonObject.getString("session_key");
        redisUtils.set(openid, session_key);

        UserParam userParam = new UserParam();
        userParam.setUserId(openid);
        userParam.setUserAddress("暂无地址");
        if (!orderService.ifExistUser(openid)){
            try {
                userService.insertUser(userParam);
            } catch (Exception e) {
                log.error("插入用户表失败！" + e);
            }
        }
        
        return openid;


        /*CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpGet get = new HttpGet(openIdurl);
        CloseableHttpResponse response = client.execute(get);
        HttpEntity result = response.getEntity();
        String content = EntityUtils.toString(result);
        JSONObject jsonObject = JSONObject.parseObject(content);
        String openid = jsonObject.getString("openid");
        String session_key = jsonObject.getString("session_key");
        redisUtils.set(openid, session_key);
        UserParam userParam = new UserParam();
        userParam.setUserId(openid);
        userParam.setUserAddress("暂无地址");
        if (!orderService.ifExistUser(openid)){
            try {
                userService.insertUser(userParam);
            } catch (Exception e) {
                log.error("插入用户表失败！" + e);
            }
        }
        return openid;*/
    }

    //http://localhost:8080/wx/wxLogin
    @ApiOperation("直接微信登录api")
    @GetMapping("/wxLogin")
    public void wxLogin(HttpServletResponse response) throws IOException {
        String codeUrl = wxConfigure.getCode();
        response.sendRedirect(codeUrl);
    }

    @ApiOperation("微信登录api回调，请勿手动调用")
    @PostMapping("/wxLoginBack")
    public String wxLoginBack(String code) throws IOException {
        //根据code拿 access_token以及 openid
        String codeBackString = HttpClientUtils.doGet(wxConfigure.getAccessToken(code));
        JSONObject codeBack = JSONObject.parseObject(codeBackString);
        String access_token = codeBack.getString("access_token");
        String openid = codeBack.getString("openid");

        //根据 access_token拿用户信息
        String tokenBackString = HttpClientUtils.doGet(wxConfigure.getUserInfo(access_token, openid));
        log.info(tokenBackString);
        //设置本系统token
        redisUtils.set(openid, tokenBackString, 1800);
        //存入数据库
        Map<String, Object> userInfoMap = new HashMap<String, Object>();
        userInfoMap = JSON.parseObject(tokenBackString, Map.class);
        UserParam userParam = new UserParam();
        userParam.setUserId(userInfoMap.get("openid").toString());
        userParam.setUserName(userInfoMap.get("nickname").toString());
        userParam.setUserPhone("");
        userParam.setUserPic(userInfoMap.get("headimgurl").toString());
        userParam.setUserAddress(userInfoMap.get("province").toString());
        try {
            userService.insertUser(userParam);
        } catch (Exception e) {
            log.error("插入用户表失败！" + e);
        }

        //将token返回前台（测试状态）
        //todo
        return "redirect:https://www.baidu.com?" + openid;
    }

    @PostMapping(value = "/createUrl")
    public ReturnResult createUrl(@RequestParam String url) throws Exception {
        String content = url;
        //String filePath = "/home/jialin/file/img/qrCode/qrCode.png";
        String filePath = "D://qrCode.png";
        try {
            QRCodeUtil.createQrCode(new FileOutputStream(new File(filePath)), content, 900, "png");
            return ReturnResultUtils.returnSuccess("创建二维码图片成功！");
        } catch (Exception e) {
            log.error("创建二维码图片失败！");
            return ReturnResultUtils.returnFailMsg(612, "创建二维码图片失败！");
        }
    }

}
