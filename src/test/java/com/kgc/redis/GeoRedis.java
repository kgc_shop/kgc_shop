package com.kgc.redis;

import com.kgc.ShopApplication;
import com.kgc.shop.params.OutletInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.*;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.connection.RedisGeoCommands.GeoLocation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jialin
 * @createTime 2020-10-12
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShopApplication.class)
public class GeoRedis {
    /**
     * geoadd
     * geopos
     * geodist
     * georadius
     * georadiusbymember
     */
    @Autowired
    private RedisTemplate redisTemplate;

    //geoadd 向Redis中添加位置信息
    //1.网点位置信息，geo
    //2.网点详细信息，hash
    @Test
    public void testRedisGeoAdd() {
        //1.网点位置信息存储
        Point point = new Point(112.210719, 32.092452);
        redisTemplate.boundGeoOps("outlets").geoAdd(point, "中国建设银行(襄州支行)");
        //网点详细信息，hash
        OutletInfo outletInfo = new OutletInfo("中国建设银行(襄州支行)",
                "襄阳市襄州区交通路与航空路交叉路口向北约50米",
                "15189805605",
                112.210719, 32.092452);
        redisTemplate.boundHashOps("outletsInfo").put("中国建设银行(襄州支行)", outletInfo);
    }

    //批量添加
    @Test
    public void testGeoAddMulti() {
        Map<String, Point> outletMap = new HashMap<>();
        //存储网点详细信息
        OutletInfo outletInfoTemp = new OutletInfo(
                "中国建设银行(江南支行)",
                "湖北省襄阳市襄城南街119号",
                "(0710)2811039",
                112.163033, 32.917786
        );
        redisTemplate.boundHashOps("outletsInfo").put(outletInfoTemp.getTitle(), outletInfoTemp);
        Point point = new Point(outletInfoTemp.getX(), outletInfoTemp.getY());
        outletMap.put(outletInfoTemp.getTitle(), point);
        outletInfoTemp = new OutletInfo(
                "中国建设银行(宜城支行)",
                "湖北省襄阳市宜城市鄢城街道自忠路148号建行营业部",
                "", 112.268983, 31.719637
        );
        redisTemplate.boundHashOps("outletsInfo").put(outletInfoTemp.getTitle(), outletInfoTemp);
        point = new Point(outletInfoTemp.getX(), outletInfoTemp.getY());
        outletMap.put(outletInfoTemp.getTitle(), point);
        outletInfoTemp = new OutletInfo(
                "中国建设银行(襄阳襄东支行)",
                "湖北省褒阳市褒州区航空路43号",
                " (0710)3712330", 112.190265, 32.086621
        );
        redisTemplate.boundHashOps("outletsInfo").put(outletInfoTemp.getTitle(), outletInfoTemp);
        point = new Point(outletInfoTemp.getX(), outletInfoTemp.getY());
        outletMap.put(outletInfoTemp.getTitle(), point);
        outletInfoTemp = new OutletInfo(
                "中国建设银行(老河口支行营业部)",
                "湖北省襄阳市老河口市光化街道胜利路22号老河口建行",
                "(0710)8221926, (0710)8223514", 111.683041, 32.393239
        );
        redisTemplate.boundHashOps("outletsInfo").put(outletInfoTemp.getTitle(), outletInfoTemp);
        point = new Point(outletInfoTemp.getX(), outletInfoTemp.getY());
        outletMap.put(outletInfoTemp.getTitle(), point);
        outletInfoTemp = new OutletInfo(
                "中国建设银行(襄阳汉江路支行)",
                "湖北省襄阳市樊城区汉江街道汉江路18号襄阳建行",
                "", 112.133266, 32.050267
        );
        redisTemplate.boundHashOps("outletsInfo").put(outletInfoTemp.getTitle(), outletInfoTemp);
        point = new Point(outletInfoTemp.getX(), outletInfoTemp.getY());
        outletMap.put(outletInfoTemp.getTitle(), point);
        redisTemplate.boundGeoOps("outlets").geoAdd(outletMap);
    }

    //geopos 返回位置的坐标信息
    @Test
    public void testGeoPos() {
        List<Point> outlets = redisTemplate.boundGeoOps("outlets").geoPos("中国建设银行(襄州支行)");
        for (Point outlet : outlets) {
            System.out.println("中国建设银行（襄州支行）坐标是：经度： " + outlet.getX() + " 纬度： " + outlet.getY());
        }
    }

    //geodist 计算两个位置之间的距离
    @Test
    public void testGeoDist() {
        Distance distance = redisTemplate.boundGeoOps("outlets").geoDist(
                "中国建设银行(老河口支行营业部)",
                "中国建设银行(江南支行)"
        );
        double val = distance.getValue();
        String unit = distance.getUnit();
        System.out.println("两点相距：" + val + " " + unit);
        //以公里显示
        Distance distance1 = redisTemplate.boundGeoOps("outlets").geoDist(
                "中国建设银行(老河口支行营业部)",
                "中国建设银行(江南支行)",
                Metrics.KILOMETERS
        );
        double val1 = distance1.getValue();
        String unit1 = distance1.getUnit();
        System.out.println("两点相距：" + val1 + " " + unit1);
        //以公里显示,小数点保留两位
        Distance distance2 = redisTemplate.boundGeoOps("outlets").geoDist(
                "中国建设银行(老河口支行营业部)",
                "中国建设银行(江南支行)",
                Metrics.KILOMETERS
        );
        double val2 = distance2.getValue();
        String unit2 = distance2.getUnit();
        System.out.println("两点相距：" + new BigDecimal(val2).setScale(2, BigDecimal.ROUND_HALF_UP) + " " + unit2);
    }

    //georadius 按照给定的经纬度查找指定范围的位置
    @Test
    public void testGeoRadius() {
        //构建中心点和指定距离
        Point point = new Point(112.163033, 32.917786);
        Distance distance = new Distance(100, Metrics.KILOMETERS);
        Circle circle = new Circle(point, distance);
        RedisGeoCommands.GeoRadiusCommandArgs args = RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs();
        args.includeDistance();
        //results存放的是查询到的网点位置信息
        GeoResults<GeoLocation<String>> results = redisTemplate.boundGeoOps("outlets").geoRadius(circle, args);
        for (GeoResult<GeoLocation<String>> result : results) {
            GeoLocation<String> location = result.getContent();
            String name = location.getName();
            //获取距离
            Distance dis = result.getDistance();
            String val = new BigDecimal(dis.getValue()).setScale(2, BigDecimal.ROUND_HALF_UP) + dis.getUnit();
            System.out.println("112.163033,32.917786距离" + name + "网点" + val + "公里");
        }
    }
    
    //georadiusbymember 按照给定元素（必须在集合中存在）查找指定范围的位置
    @Test
    public void testGeoRadiusByMember(){
        Distance distance = new Distance(5, Metrics.KILOMETERS);
        RedisGeoCommands.GeoRadiusCommandArgs args = RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs();
        args.includeDistance();
        GeoResults<GeoLocation<String>> results = redisTemplate.boundGeoOps("outlets").geoRadiusByMember("中国建设银行(江南支行)", distance, args);
        for (GeoResult<GeoLocation<String>> result : results) {
            GeoLocation<String> location = result.getContent();
            String name = location.getName();
            //获取距离
            Distance dis = result.getDistance();
            String val = new BigDecimal(dis.getValue()).setScale(2, BigDecimal.ROUND_HALF_UP) + dis.getUnit();
            System.out.println("中国建设银行(江南支行)距离" + name + "网点" + val + "公里");
        }
    }
    
}
